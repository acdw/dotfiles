#!/usr/bin/env bash
# cf https://github.com/baskerville/bin/blob/master/bed
# changes:
# - no finding of files -- baskerville's will look for files if there's no
#   exact match, but I don't want to do that
# - more options -- to create scripts, remove scripts, create in a directory

usage() { script=$(basename $0); cat <<END
$script : Bin EDit
adapted from baskerville@github

USAGE: $script [-h] [-c] [-r] [-d DIR] SCRIPT [SCRIPT...]
Edit SCRIPTs from your \$XDG_BIN_DIR.

-h:     Show this help.
-c:     If SCRIPT does not exist, create it.
-r:     Remove SCRIPT from \$PATH.
-d DIR: Save created scripts to DIR.  Implies -c.

END
exit ${1:-1}
}

# defaults
CREATE_FILES=false
REMOVE_FILES=false
BIN_DIR="${XDG_BIN_DIR:-$HOME/.local/bin}"

while getopts 'hcrd:' opt
do
    case "$opt" in
        h) # help
            usage
            ;;
        c) # create scripts
            CREATE_FILES=true
            ;;
        r) # remove scripts
            REMOVE_FILES=true
            ;;
        d) # directory to save scripts to
            CREATE_FILES=true
            BIN_DIR="$OPTARG"
            if [[ ! -d "$BIN_DIR" ]]
            then
                echo "$BIN_DIR is not a valid directory." >&2
                exit 3
            fi
            ;;
        \?) 
            echo "Invalid option: -$OPTARG" >&2
            usage 2 >&2
            ;;
        :)
            echo "Option -$OPTARG needs a parameter" >&2
            usage 2 >&2
            ;;
    esac
done
shift $((OPTIND-1))

# edit the files
while (( $# > 0 ))
do
    pattern="$1"
    target="$(which "$pattern" 2>/dev/null)"
    if (( $? == 0 )) # target found
    then
        if $REMOVE_FILES
        then
            read -p "Remove $target (y/N)? " yn
            case "$yn" in
                y*|Y*) rm "$target" && echo "$target removed." ;;
                *) echo "Nevermind." ;;
            esac
        else
            editor "$target"
        fi
    else # target not found
        if $CREATE_FILES
        then
            file="$BIN_DIR/$pattern"
            editor "$file"
            [[ -f "$file" ]] && chmod +x "$file"
        else
            echo "$pattern is not in \$PATH" >&2
        fi
    fi
    shift
done
