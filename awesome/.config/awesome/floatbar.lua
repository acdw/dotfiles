--       __ _             _   _
--      / _| | ___   __ _| |_| |__   __ _ _ __
--     | |_| |/ _ \ / _` | __| '_ \ / _` | '__|
--     |  _| | (_) | (_| | |_| |_) | (_| | |
--     |_| |_|\___/ \__,_|\__|_.__/ \__,_|_|
--
--
-- Show titlebar only for floating clients.
-- Initial script author: Niverton (https://stackoverflow.com/a/44120615). Thank
-- you Niverton!

-- Copyright (C) 2018 pppp
--
-- This program is free software: you can redistribute it and/or modify it
-- under the terms of the GNU General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option)
-- any later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
-- FITNESS FOR A PARTICULAR PURPOSE.    See the GNU General Public License for
-- more details.
--
-- You should have received a copy of the GNU General Public License along with
-- this program.    If not, see <https://www.gnu.org/licenses/>.
--

local awful = require("awful")
local capi = {
    awesome = awesome,
    client = client,
    tag = tag,
}

local floatbar = {}

-- local function debug(msg)
--     require('naughty').notify { text = msg }
-- end

local isfloating = function(c, t)
    if not t then t = c.first_tag end
    if not t then return false end
    if c.floating and not c.fullscreen then
        return true
    elseif t.layout == awful.layout.suit.floating or t.layout == nil then
        -- apparently, if the layout isn't set on creating a tag then it's not
        -- pre-populated. XXX this is a shim until I get my tag-creating
        -- defaults configurated, which should fix this problem.
        return true
    else
        return false
    end
end

function floatbar.init()
    capi.awesome.register_xproperty("_COMPTON_SHADOW", "number")
    local set_titlebar = function(c, s)
        if s then
            -- On restarts, the client requests titlebars before 'remembering'
            -- the geometry, so it gradually gets smaller and smaller over
            -- time.  Thus the fix.
            local geom_fix = true
            if c.titlebar == nil then
                c:emit_signal("request::titlebars", "rules", {})
                geom_fix = false
            end
            -- TODO this still doesn't work for clients that are set floating
            -- (vs floating through layout.suit.floating)
            if geom_fix then c:geometry(c.floating_geometry) end
            awful.titlebar.show(c)
            c:set_xproperty("_COMPTON_SHADOW", 1)
            awful.placement.no_offscreen(c)
        else
            c.floating_geometry = c:geometry()
            awful.titlebar.hide(c)
            c:set_xproperty("_COMPTON_SHADOW", 0)
        end
    end

    capi.client.connect_signal("property::floating", function(c)
        set_titlebar(c, isfloating(c))
    end)

    capi.client.connect_signal("property::fullscreen", function(c)
        set_titlebar(c, isfloating(c))
        c:emit_signal("request::geometry", "fullscreen", { store_geometry = false })
    end)

    capi.client.connect_signal("tagged", function(c)
        set_titlebar(c, isfloating(c))
    end)

    capi.tag.connect_signal("property::layout", function(t)
        for _, c in pairs(t:clients()) do
            set_titlebar(c, isfloating(c, t))
        end
    end)
end

return floatbar
