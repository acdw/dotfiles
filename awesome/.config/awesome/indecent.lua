-- INDECENT
-- manipulate tags based on numerical indeces
-- we can also create new tags at the end of the current list if one doesn't
-- exist
-- There are a few options as to how to create nonexistent tags:
-- -- if I push "M-9" and my tags are {1, 2, 3}, which do I want:
-- -- -- (a) create tag "4" in the fourth position ("M-4" will access)
-- -- -- -- -- this means that multiple "M-9"s will create tags up to 9
-- -- -- (b) create tag "9" in the fourth position (future "M-4" will access)
-- -- -- -- -- future "M-9"s will create more "9" tags
-- -- -- (c) create tag "9" so that "M-9" will always access it
--
-- I've decided on (c), with a slight twist.  Say the tags are thus:       
-- main     net     3
-- If I hit "M-9" at this point, it will be created:
-- main     net     3       9
-- After that, "M-9" will access tag "9".
-- However, "9" will be the fourth tag on the screen, so it will also be
-- accessible via "M-4".  While this is a little awkward, I think if the user
-- really wants a tag "4" they can create it themselves.  I'm trying to use
-- tags in this way to move away from plain numbered tags in favor of semantic
-- tag names, anyway.  
-- At some point, who knows?  Maybe I'll provide a prompt to name tags as
-- they're created too.  Or maybe using the number keys to index tags isn't
-- the best idea anyway.

local awful = require "awful"

local module = {}

-- common functionality
local function sel_or_add_idx(i, s, create)
    local t
    local ts = s.tags
    if i <= #ts then
        t = ts[i]
    elseif awful.tag.find_by_name(s, tostring(i)) then
        t = awful.tag.find_by_name(s, tostring(i))
    elseif create then
        -- local i = #ts + 1
        t = awful.tag.add(tostring(i),
            { screen = s, index = i, 
              volatile = true, 
              layout = awful.layout.layouts[1]})
    else return nil
    end
    return t
end

-- view tag at index
module.view_tag_idx = function (i, new)
    local s = awful.screen.focused()
    local t = sel_or_add_idx(i, s, new == nil and true or new)
    if t then
        t:view_only()
    end
end

-- toggle visibility of the tag at index
module.toggle_tag_idx = function (i, new)
    local s = awful.screen.focused()
    local t = sel_or_add_idx(i, s, new == nil and true or new)
    if t then
        awful.tag.viewtoggle(t)
    end
end

module.move_focused_tag_idx = function (i, new)
    local c = client.focus
    if c then
        local s = c.screen
        local t = sel_or_add_idx(i, s, new == nil and true or new)
        if t then
            c:move_to_tag(t)
        end
    end
end

module.toggle_focused_tag_idx = function (i, new)
    local c = client.focus
    if c then
        local s = c.screen
        local t = sel_or_add_idx(i, s, new == nil and true or new)
        if t then
            c:toggle_tag(t)
        end
    end
end

return module
