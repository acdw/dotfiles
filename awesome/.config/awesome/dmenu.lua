-- Dmenu.lua
-- for awesome

local posix = require("posix")
local io = require("io")

-- cf. https://stackoverflow.com/questions/1242572/
local function popen3(path, ...)
    local r1, w1 = posix.pipe()
    local r2, w2 = posix.pipe()
    local r3, w3 = posix.pipe()

    assert((w1 ~= nil or r2 ~= nil or r3 ~= nil), "pipe() failed")

    local pid, err = posix.fork()
    assert(pid ~= nil, "fork() failed")
    if pid == 0 then
        posix.close(w1)
        posix.close(r2)
        posix.dup2(r1, posix.fileno(io.stdin))
        posix.dup2(w2, posix.fileno(io.stdout))
        posix.dup2(w3, posix.fileno(io.stderr))
        posix.close(r1)
        posix.close(w2)
        posix.close(w3)

        local ret, err = posix.execp(path, ...)
        assert(ret ~= nil, "execp() failed")

        posix.exit(1)
        return
    end
    posix.close(r1)
    posix.close(w2)
    posix.close(w3)

    return pid, w1, r2, r3
end

local function pipe_simple(input, cmd, ...)
    -- launch child process
    local pid, w, r, e = popen3(cmd, ...)
    assert(pid ~= nil, "filter() unable to popen3()")

    -- Write to popen3's stdin and close it
    posix.write(w, input)
    posix.close(w)

    local bufsize = 4096

    -- Read popen3's stdout
    local stdout = {}
    local i = 1
    while true do
        buf = posix.read(r, bufsize)
        if buf == nil or #buf == 0 then break end
        stdout[i] = buf
        i = i + 1
    end
    posix.close(r)

    -- Read popen3's stderr
    local stderr = {}
    local i = 1
    while true do
        buf = posix.read(e, bufsize)
        if buf == nil or #buf == 0 then break end
        stderr[i] = buf
        i = i + 1
    end
    posix.close(e)

    -- Clean up child and get return code
    local wait_pid, wait_cause, wait_status = posix.wait(pid)
    return wait_status, table.concat(stdout), table.concat(stderr)
end

local function split(input, sep)
    if sep == nil then sep = "%s" end
    local t = {}
    local i = 1
    for s  in string.gmatch(input, "([^"..sep.."]+)") do
        t[i] = s
        i = i + 1
    end
    return t
end

dmenu = {}

-- dmenu binary. This can be anything that takes dmenu syntax
-- (like, for example, rofi -dmenu)
dmenu.cmd = '/usr/bin/dmenu'

-- arguments map, to make it easier to read what we're doing.
-- Update this table with whatever the arguments of your dmenu are.
dmenu.args_map = {
--  name          flag    default
    ['bottom']  = { '-b' , false },
    ['fast']    = { '-f' , false },
    ['nocase']  = { '-i' , false },
    ['lines']   = { '-l' , -1 }   ,
    ['monitor'] = { '-m' , -1 }   ,
    ['prompt']  = { '-p' , '' }   ,
    ['font']    = { '-fn', '' }   ,
    ['normbg']  = { '-nb', '' }   ,
    ['normfg']  = { '-nf', '' }   ,
    ['selbg']   = { '-sb', '' }   ,
    ['selfg']   = { '-sf', '' }   ,
    ['embed']   = { '-w' , '' }   ,
}

function dmenu.run(items, dmenu_args, permissive)
    -- if dmenu.cmd has a space, it needs to be split up
    local args = split(dmenu.cmd)
    local cmd = table.remove(args, 1)
    -- allow the user to choose an item that isn't defined?
    if permissive == nil then permissive = false end
    -- but if this function is run without items, be permissive
    if not items or next(items) == nil then permissive = true end

    -- process dmenu's arguments
    -- dmenu_args table looks like this: { bottom = true, prompt = ">> " }
    if dmenu_args then
        for k, v in pairs(dmenu_args) do
            -- if the argument is a valid one...
            if dmenu.args_map[k] then
                -- if the value isn't the default...
                if v ~= dmenu.args_map[k][2] then
                    -- add it to the command line
                    table.insert(args, dmenu.args_map[k][1])
                    -- if it's not a bool, add the value too
                    if type(v) ~= 'boolean' then
                        table.insert(args, tostring(v))
                    end
                end
            end
        end
    end

    -- process items
    -- items table is thus: { item1 = function1, item2 = function2, ... }
    -- the keys to the dictionary will be given to dmenu, it will be run, and
    -- the result passed back.  Then we'll execute the function associated
    -- with the key, or, if the key doesn't exist in the table, just quit.
    -- XXX: the items will not be in order because of lua's tables.
    -- -- this could be solved with a structure { { item, func }, ... }
    -- -- but that would be different. TO CONSIDER.
    if not permissive and (not items or next(items) == nil) then
        -- if there are no items, there's nothing to do.
        return 
    end

    local input = ""
    -- combine the keys into a string for dmenu
    if items then
        for k, _ in pairs(items) do
            input = input .. k .. '\n'
        end
    end
    -- run dmenu
    dmenu.stat, dmenu.out, dmenu.err = pipe_simple(input, cmd, args)
    dmenu.out = dmenu.out:gsub('\n$', '')

    -- if there were errors, quit
    assert(dmenu.stat == 0, 'dmenu returned an error code.')
    -- if we're not allowing just any input, then
    if not permissive then
        -- check for a valid choice
        assert(items[dmenu.out], 'A valid choice was not given.')
        -- call the function associated with the key
        return items[dmenu.out]()
    else
        -- if it's a defined item then return the function
        if items and items[dmenu.out] ~= nil then 
            if type(items[dmenu.out]) == "function" then
                return items[dmenu.out]() 
            else
                return items[dmenu.out]
            end
        end
        -- otherwise, just return the string
        return dmenu.out
    end
end

return dmenu
