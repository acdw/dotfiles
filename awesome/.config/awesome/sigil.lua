-- sigil
-- an easy way to setup signals
-- because why not?

local sigil = {}
local capi = {
    awesome = awesome,
    client = client,
    screen = screen,
    tag = tag,
}

-- the signal table. it looks like this:
-- { screen = { ["property::geometry"] = set_wallpaper, ... }, ... }
sigil.signals = {}

-- replacement for obj.connect_signal
sigil.connect_signal = function (obj, signal, func)
    capi[obj].connect_signal(signal, func)
end

-- replacement for obj.disconnect_signal
sigil.disconnect_signal = function (obj, signal, func)
    capi[obj].disconnect_signal(signal, func)
end

-- connect all signals in sigil.signals
sigil.connect_all = function (list, disconnect_first)
    -- should we disconnect existing signals? defaults to false.
    local disconnect_first = disconnect_first or false
    local list = type(list) == "table" and list or sigil.signals or {}

    local connect_signal = function (obj, sig, func)
        if disconnect_first then signal.disconnect_signal(obj, sig, func) end
        sigil.connect_signal(obj, sig, func)
    end

    for obj, sigs in pairs(list) do
        for sig, func in pairs(sigs) do
            connect_signal(obj, sig, func)
        end
    end
end

return sigil
