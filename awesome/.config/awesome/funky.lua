-- funky functions that don't fit anywhere else
local beautiful = require "beautiful"
local gears = require "gears"
local awful = require "awful"
local capi = {
    client = client,
    screen = screen,
    tag = tag,
}
require "shadows"

local module = {}

local function get_screen(s)
    return s and capi.screen[s]
end

-- set the wallpaper. from the example rc.lua
function module.set_wallpaper (screen)
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(screen)
        end
        gears.wallpaper.maximized(wallpaper, screen, true)
    end
end

-- swap focus with last-focused client. from example rc.lua
function module.focus_swap ()
    awful.client.focus.history.previous()
    if capi.client.focus then
        capi.client.focus:raise()
    end
end

-- restore a (random) minimized client.  from example rc.lua
function module.restore_client ()
    local c = awful.client.restore()
    if c then
        capi.client.focus = c
        c:raise()
    end
end

-- toggle a client's property
function module.toggle_prop (client, property)
    client[property] = not client[property]
    client:raise()
end

return module
