-- std lib
local gears = require "gears"
local awful = require "awful"
require "awful.autofocus"
-- widgets
local wibox = require "wibox"
-- themes
local beautiful = require "beautiful"
-- notifications
local naughty = require "naughty"
-- extras
local ezconfig = require "ezconfig"
local floatbar = require "floatbar"
-- local memorial = require "memorial"
local indecent = require "indecent"
local funky = require "funky"
require "shadows" -- replace some standard library functionality
local sigil = require "sigil"
------
local switcher = require "awesome-switcher"
local revelation = require "awesome-revelation"

-- handle runtime errors
do
    local in_error = false
    sigil.connect_signal("awesome", "debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true
        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

-- variable definitions
-- -- theme
beautiful.init(gears.filesystem.get_configuration_dir() .. "theme.lua")
revelation.init()
-- -- programs
local terminal = "terminal"
local editor = os.getenv("EDITOR") or "editor"
local wbrowser = "qutebrowser"
local fbrowser = "rox"
-- -- modkey
local modkey = "Mod4"
ezconfig.modkey = modkey
local btns = {}
local keys = {}
-- -- layouts for awful.layout.inc; order matters
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.max,
}

-- helper functions -- now in funky.lua

-- wibar
-- Create a wibox for each screen and add it
-- buttons for the taglist
btns.bar = {}
btns.bar.taglist = ezconfig.btntable.join {
    ["1"] = function(t) t:view_only() end,
    ["M-1"] = 
        function(t) 
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end,
    ["3"] = awful.tag.viewtoggle,
    ["M-3"] =
        function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end,
    ["4"] = function(t) awful.tag.viewnext(t.screen) end,
    ["5"] = function(t) awful.tag.viewprev(t.screen) end,
}
-- buttons for the tasklist
btns.bar.tasklist = ezconfig.btntable.join {
    ["1"] = 
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                c.minimized = false
                if not c:isvisible()
                   and c.first_tag then
                    c.first_tag:view_only()
               end
               client.focus = c
               c:raise()
            end
        end,
    ["4"] = { awful.client.focus.byidx, 1 },
    ["5"] = { awful.client.focus.byidx, -1 },
}
-- buttons for the layout widget
btns.bar.layout = ezconfig.btntable.join {
    ["1"] = { awful.layout.inc, 1 },
    ["3"] = { awful.layout.inc, -1 },
    ["4"] = { awful.layout.inc, 1 },
    ["5"] = { awful.layout.inc, -1 },
}

-- Re-set wallpaper when a screen's geometry changes (e.g. diff resolution)
sigil.signals.screen = {
    ["property::geometry"] = funky.set_wallpaper,
}

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    funky.set_wallpaper(s)

    -- Tags (just set the one, for now)
    awful.tag({ "★" }, s, awful.layout.layouts[1])

    s.bar = {}
    -- Create a promptbox for each screen
    s.bar.prompt = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon 
    -- indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.bar.layout = awful.widget.layoutbox(s)
    s.bar.layout:buttons(btns.bar.layout)
    -- Create a taglist widget
    s.bar.taglist = awful.widget.taglist(s, 
        awful.widget.taglist.filter.all,
        btns.bar.taglist)

    -- Create a tasklist widget
    s.bar.tasklist = awful.widget.tasklist(s,
        awful.widget.tasklist.filter.currenttags,
        btns.bar.tasklist)

    -- Create the wibox
    s.bar.bar = awful.wibar({ position = "top", screen = s, height = 18 })

    -- Add widgets to the wibox
    s.bar.bar:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- mylauncher,
            s.bar.taglist,
            s.bar.prompt,
        },
        s.bar.tasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            -- mykeyboardlayout,
            wibox.widget.systray(),
            wibox.widget.textclock(),
            s.bar.layout,
        },
    }
end)

-- mouse bindings
btns.root = ezconfig.btntable.join {
    ["4"] = awful.tag.vewnext,
    ["5"] = awful.tag.viewprev,
}
btns.client = ezconfig.btntable.join {
    ["1"] = function (c) client.focus = c; c:raise() end,
    ["M-1"] = awful.mouse.client.move,
    ["M-3"] = awful.mouse.client.resize,
    ["M-C-1"] = awful.mouse.client.resize,
}

-- key bindings
keys.root = ezconfig.keytable.join {
    -- focus tags
    ['M-<Left>'] = awful.tag.viewprev,
    ['M-<comma>'] = awful.tag.viewprev,
    ['M-<Right>'] = awful.tag.viewnext,
    ['M-<period>'] = awful.tag.viewnext,
    ['M-<grave>'] = awful.tag.history.restore,
    -- focus clients
    ['M-j'] = { awful.client.focus.byidx, 1 },
    ['M-k'] = { awful.client.focus.byidx, -1 },
    ['M-u'] = awful.client.urgent.jumpto,
    -- ['M-<Tab>'] = funky.focus_swap,
    ['M-C-n'] = funky.restore_client,
    ['M-<Tab>'] = revelation.expose,
    ['A-<Tab>'] = { switcher.switch, 1, "Mod1", "Alt_L", "Shift", "Tab" },
    ['A-S-<Tab>'] = { switcher.switch, -1, "Mod1", "Alt_L", "Shift", "Tab" },
    -- focus screens
    ['M-C-j'] = { awful.screen.focus_relative, 1 },
    ['M-C-k'] = { awful.screen.focus_relative, -1 },
    -- shift clients
    ['M-S-j'] = { awful.client.swap.byidx, 1 },
    ['M-S-k'] = { awful.client.swap.byidx, -1 },
    -- layout manipulation
    ['M-l'] = { awful.tag.incmwfact, 0.05 },
    ['M-h'] = { awful.tag.incmwfact, -0.05 },
    ['M-S-h'] = { awful.tag.incnmaster, 1, nil, true },
    ['M-S-l'] = { awful.tag.incnmaster, -1, nil, true },
    ['M-C-h'] = { awful.tag.incncol, 1, nil, true },
    ['M-C-l'] = { awful.tag.incncol, -1, nil, true },
    ['M-<space>'] = { awful.layout.inc, 1 },
    ['M-S-<space>'] = { awful.layout.inc, -1 },
    ['M-#86'] = { -- KP_Add
        -- awful.layout.inc, 1, awful.screen.focused(), {
        -- }
    },
    ['M-#82'] = { -- KP_Subtract
        -- awful.layout.inc, 1, awful.screen.focused(), {
        -- }
    },
    ['M-#63'] = --{ -- KP_Multiply: switch between max and floating
    function()
        scr = awful.screen.focused()
        awful.layout.inc(1, scr, {
            awful.layout.suit.max,
            awful.layout.suit.floating,
        })
    end,
    -- },
    ['M-#106'] = --{ -- KP_Divide: switch between tile algos
    function()
        scr = awful.screen.focused()
        awful.layout.inc(1, awful.screen.focused(), {
           awful.layout.suit.tile,
           awful.layout.suit.tile.bottom, 
        })
    end,
    --},
    -- awesomewm
    ['M-C-r'] = awesome.restart,
    ['M-S-q'] = awesome.quit,
    -- launch
    ['M-<Return>'] = { awful.spawn, 'terminal' },
    ['M-r'] = { awful.spawn, 'rofi -show run' },
}
keys.client = ezconfig.keytable.join {
    ["M-f"] = function (c) funky.toggle_prop(c, 'fullscreen') end,
    ["M-S-c"] = function (c) c:kill() end,
    ["M-C-<space>"] = function (c) funky.toggle_prop(c, 'floating') end,
    ["M-C-<Return>"] = function (c) c:swap(awful.client.getmaster()) end,
    ["M-o"] = function (c) c:move_to_screen() end,
    ["M-t"] = function (c) funky.toggle_prop(c, 'ontop') end,
    ["M-n"] = function (c) c.minimized = true end,
    ["M-m"] = function (c) funky.toggle_prop(c, 'maximized') end,
    ["M-C-m"] = function (c) funky.toggle_prop(c, 'maximized_vertical') end,
    ["M-S-m"] = function (c) funky.toggle_prop(c, 'maximized_horizontal') end,
}
-- number keys
for i = 1, 9 do
    keycode = '#' .. i + 9
    keys.root = gears.table.join(keys.root, ezconfig.keytable.join {
        ["M-"..keycode] = { indecent.view_tag_idx, i },
        ["M-C-"..keycode] = { indecent.toggle_tag_idx, i },
        ["M-S-"..keycode] = { indecent.move_focused_tag_idx, i },
        ["M-S-C-"..keycode] = { indecent.toggle_focused_tag_idx, i },
    })
end

-- rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = keys.client,
                     buttons = btns.client,
                     screen = awful.screen.preferred,
                     placement = 
                        awful.placement.no_overlap + 
                        awful.placement.no_offscreen
     }
    },
    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Wpa_gui",
          "XCalc",
          "mpv",
          "pinentry",
          "veromix",
          "xtightvncviewer",
        },
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
        }, 
        properties = { floating = true }
      },
    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "dialog" } }, 
      properties = { titlebars_enabled = true }
    },
    -- Center viewers
    { rule_any = {
        class = { 
            "mpv",
            "Sxiv", 
            "keepassxc", 
        },
    },
        properties = {
            floating = true,
            placement = awful.placement.centered,
        },
    },
    -- net workspace
    { rule_any = { 
        class = { "qutebrowser", "Firefox" },
    },
      properties = { 
          tag = "net", 
          layout = awful.layout.suit.max,
          switchtotag = true,
        }
    },
    { rule = { class = "URxvt", name = "rtv", },
      properties = { 
          tag = "net",
      }
    },
}

-- Titlebar function
function titlebar(c)
    local buttons = ezconfig.btntable.join {
        ["1"] = 
            function() 
                client.focus = c
                c:raise()
                awful.mouse.client.move(c)
            end,
        ["3"] = 
            function()
               client.focus = c
               c:raise()
               awful.mouse.client.resize(c)
            end,
    }
    awful.titlebar(c, {size=16}) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout = wibox.layout.fixed.horizontal,
        },
        { -- Middle
            { -- Title
                widget = awful.titlebar.widget.titlewidget(c),
                align = "left",
            },
            buttons = buttons,
            layout = wibox.layout.fixed.horizontal,
        },
        { -- Right
            -- awful.titlebar.widget.floatingbutton(c),
            awful.titlebar.widget.minimizebutton(c),
            awful.titlebar.widget.maximizedbutton(c),
            -- awful.titlebar.widget.stickybutton(c),
            -- awful.titlebar.widget.ontopbutton(c),
            awful.titlebar.widget.closebutton(c),
            layout = wibox.layout.fixed.horizontal,
        },
        layout = wibox.layout.align.horizontal,
    }
end

-- signals
sigil.signals.client = {
    ['manage'] = 
        function (c)
            -- Set the windows at the slave,
            -- i.e. put it at the end of others instead of setting it master.
            -- if not awesome.startup then awful.client.setslave(c) end
            if awesome.startup 
               and not c.size_hints.user_position
               and not c.size_hints.program_position then
                -- Prevent clients from being unreachable after screen count
                -- changes.
                awful.placement.no_offscreen(c)
            end
        end,
    ['request::titlebars'] = function(c) titlebar(c) end,
    ['mouse::enter'] = 
        function(c)
            if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
               and awful.client.focus.filter(c) then
                client.focus = c
            end
        end,
    ['focus'] = function(c) c.border_color = beautiful.border_focus end,
    ['unfocus'] = function(c) c.border_color = beautiful.border_normal end,
}

-- Apply everything
root.buttons(btns.root)
root.keys(keys.root)
sigil.connect_all()
floatbar.init()
