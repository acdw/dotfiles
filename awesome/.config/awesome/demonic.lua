-- demonic : dmenu interfacing library
-- the goal is to have it as close to awful.prompt as possible
-- but using dmenu (because I like how it draws stuff better)

local posix = require "posix"
local io = require "io"

local demonic = {}

-- cf. https://stackoverflow.com/questions/1242572/
local function popen3(path, ...)
    local r1, w1 = posix.pipe()
    local r2, w2 = posix.pipe()
    local r3, w3 = posix.pipe()

    assert((w1 ~= nil or r2 ~= nil or r3 ~= nil), "pipe() failed")

    local pid, err = posix.fork()
    assert(pid ~= nil, "fork() failed")
    if pid == 0 then
        posix.close(w1)
        posix.close(r2)
        posix.dup2(r1, posix.fileno(io.stdin))
        posix.dup2(w2, posix.fileno(io.stdout))
        posix.dup2(w3, posix.fileno(io.stderr))
        posix.close(r1)
        posix.close(w2)
        posix.close(w3)

        local ret, err = posix.execp(path, ...)
        assert(ret ~= nil, "execp() failed")

        posix.exit(1)
        return
    end
    posix.close(r1)
    posix.close(w2)
    posix.close(w3)

    return pid, w1, r2, r3
end

local function pipe_simple(input, cmd, ...)
    -- launch child process
    local pid, w, r, e = popen3(cmd, ...)
    assert(pid ~= nil, "filter() unable to popen3()")

    -- Write to popen3's stdin and close it
    posix.write(w, input)
    posix.close(w)

    local bufsize = 4096

    -- Read popen3's stdout
    local stdout = {}
    local i = 1
    while true do
        buf = posix.read(r, bufsize)
        if buf == nil or #buf == 0 then break end
        stdout[i] = buf
        i = i + 1
    end
    posix.close(r)

    -- Read popen3's stderr
    local stderr = {}
    local i = 1
    while true do
        buf = posix.read(e, bufsize)
        if buf == nil or #buf == 0 then break end
        stderr[i] = buf
        i = i + 1
    end
    posix.close(e)

    -- Clean up child and get return code
    local wait_pid, wait_cause, wait_status = posix.wait(pid)
    return wait_status, table.concat(stdout), table.concat(stderr)
end

local function split(input, sep)
    sep = sep or "%s" -- default to whitespace
    local t = {}
    local i = 1
    for s in string.gmatch(input, "([^"..sep.."]+)") do
        t[i] = s
        i = i + 1
    end
    return t
end

-- dmenu binary.
-- Can be anything that takes dmenu arguments (i.e., `rofi -dmenu`)
demonic.command = '/usr/bin/dmenu'

-- a map between argument names, their flags, and their default values
-- change this map to reflect your dmenu's arguments and defaults
demonic.args_map = {
--  name          flag    default
    ['bottom']  = { '-b' , false },
    ['fast']    = { '-f' , false },
    ['nocase']  = { '-i' , false },
    ['lines']   = { '-l' , -1 }   ,
    ['monitor'] = { '-m' , -1 }   ,
    ['prompt']  = { '-p' , '' }   ,
    ['font']    = { '-fn', '' }   ,
    ['normbg']  = { '-nb', '' }   ,
    ['normfg']  = { '-nf', '' }   ,
    ['selbg']   = { '-sb', '' }   ,
    ['selfg']   = { '-sf', '' }   ,
    ['embed']   = { '-w' , '' }   ,
}
