-- sometimes I need to overwrite functionality from awesome's standard lib.
-- those shadowing functions go here.

local awful = require "awful"
local gears = require "gears"

-- save floating geometry better
function awful.client.floating.toggle(c)
    c = c or client.focus
    c.floating = not c.floating
    if c.floating then
        c:geometry(c.floating_geometry)
    else
        c.floating_geometry = c:geometry()
    end
end

-- allow switching to first layout in 'layouts' if the current layout isn't in
-- 'layouts'
function awful.layout.inc(i, s, layouts)
    local function get_screen(s)
        return s and screen[s]
    end
    if type(i) == "table" then
        -- Older versions of this function had arguments (layouts, i, s), but
        -- this was changed so that 'layouts' can be an optional parameter
        layouts, i, s = i, s, layouts
    end
    s = get_screen(s or awful.screen.focused())
    local t = s.selected_tag
    layouts = layouts or awful.layout.layouts
    if t then
        local curlayout = awful.layout.get(s)
        local curindex
        for k, v in ipairs(layouts) do
            if v == curlayout or curlayout._type == v then
                curindex = k
                break
            end
        end
        if not curindex then
            -- Safety net: handle cases where another reference of the layout
            -- might be given (e.g. when (accidentally) cloning it).
            for k, v in ipairs(layouts) do
                if v.name == curlayout.name then
                    curindex = k
                    break
                end
            end
        end
        if curindex then
            local newindex = gears.math.cycle(#layouts, curindex + i)
            awful.layout.set(layouts[newindex], t)
        else
            awful.layout.set(layouts[1], t)
        end
    end
end

-- create the tag specified in a client's rules iff it doesn't already exist
-- (use new_tag for always creating a new tag)
function awful.rules.high_priority_properties.tag(c, value, props)
    if value then
        if type(value) == "string" then
            t = awful.tag.find_by_name(c.screen, value)
            if t == nil then -- doesn't exist
                t = awful.tag.add(value,
                    {
                        layout = props.layout or awful.layout.layouts[1],
                        selected = props.selected or false,
                        master_width_factor = props.master_width_factor or 0.55,
                        volatile = props.volatile == nil and true or props.volatile
                    }) -- TODO: default whatevers
            end
            value = t
        end
        if c.screen ~= value.screen then
            c.screen = value.screen
            props.screen = value.screen -- In case another rule query it
        end
        c:tags{ value }
    end
end
