local tyrannical = require "tyrannical"
local awful = require "awful"

-- -- tyrannical
tyrannical.settings.default_layout = awful.layout.suit.floating
tyrannical.settings.master_width_factor = 0.55
-- -- tags
tyrannical.tags = {
    {
        name = "★",
        init = true,
        layout = awful.layout.layouts[1],
        fallback = true,
    },
    {
        name = "net",
        init = false,
        screen = screen.count() > 1 and 2 or 1,
        layout = awful.layout.suit.max,
        class = { 
            "Firefox", "nightly", "qutebrowser",
            "term:reddit",
        },
    },
    {
        name = "doc",
        init = false,
        layout = awful.layout.suit.max,
        class = { 
            "xpdf", "Xpdf", "Soffice", 
            "libreoffice-writer", "libreoffice-calc", "libreoffice-draw",
        },
    },
}
-- Ignore the tag "exclusive" property for the following clients (matched by classes)
tyrannical.properties.intrusive = {
  "ksnapshot", "pinentry", "gtksu", "kcalc", "xcalc", "feh", 
  "Gradient editor", "About KDE", "Paste Special", "Background color",
  "kcolorchooser", "plasmoidviewer", "Xephyr", "kruler",
  "plasmaengineexplorer", "keepassxc", "sxiv",
}
-- Ignore the tiled layout for the matching clients
tyrannical.properties.floating = {
  "MPlayer", "pinentry", "ksnapshot", "pinentry", "gtksu", "xine", "feh",
  "kmix", "kcalc", "xcalc", "yakuake", "Select Color$", "kruler",
  "kcolorchooser", "Paste Special", "New Form", "Insert Picture",
  "kcharselect", "mythfrontend", "plasmoidviewer"
}
-- Make the matching clients (by classes) on top of the default layout
tyrannical.properties.ontop = {
  "Xephyr", "ksnapshot", "kruler"
}
-- Force the matching clients (by classes) to be centered on the screen on init
tyrannical.properties.centered = {
  "kcalc"
}
-- Do not honor size hints request for those classes
tyrannical.properties.size_hints_honor = { 
    xterm = false, URxvt = false, aterm = false, sauer_client = false, 
    mythfrontend  = false
}
