-- memorial
-- save tags, clients, etc. over restart
-- mostly from https://github.com/pw4ever/awesome-wm-config

local awful = require "awful"

local memorial = {}

memorial.defaults = {}
-- if name is a function, it'll be called with the screen as an argument
-- e.g., memorial.defaults.name = function(s) "main" .. s end
memorial.defaults.name = "★"
memorial.defaults.layout = awful.layout.suit.floating
memorial.defaults.mwfact = 0.55
memorial.defaults.nmaster = 1
memorial.defaults.ncol = 1

-- save layouts, etc on quit/restart
-- from pw4ever
local cachedir = awful.util.get_cache_dir()
memorial.tags = cachedir .. "/awesome-tags"
memorial.client_tags = cachedir 
        .. "/awesome-client-tags-"
        .. os.getenv("XDG_SESSION_COOKIE")

-- TODO: add layout-saving by using awful.layout.get().name > file
--       and awful.layout.set(awful.layout.suit[name])
function memorial.name2layout(name, layout_t)
    if layout_t == nil then layout_t = {
        tileleft   = awful.layout.suit.tile.left,
        tilebottom = awful.layout.suit.tile.bottom,
        tiletop    = awful.layout.suit.tile.top,
        tile       = awful.layout.suit.tile.right,
        dwindle    = awful.layout.suit.spiral.dwindle,
        spiral     = awful.layout.suit.spiral,
        max        = awful.layout.suit.max,
        fullscreen = awful.layout.suit.max.fullscreen,
        magnifier  = awful.layout.suit.max.magnifier,
        floating   = awful.layout.suit.floating,
        fairh      = awful.layout.suit.fair.horizontal,
        fairv      = awful.layout.suit.fair,
        cornernw   = awful.layout.suit.corner.nw,
        cornerne   = awful.layout.suit.corner.ne,
        cornersw   = awful.layout.suit.corner.sw,
        cornerse   = awful.layout.suit.corner.se,
    } end
    return layout_t[name]
end

function memorial.save(restart)
    local scrcount = screen.count()
    -- save #screens, used for check proper tag recording
    do
        local f = io.open(memorial.tags .. ".0", "w+")
        if f then
            f:write(string.format("%d", scrcount) .. "\n")
            f:close()
        end
    end
    -- save tags
    for s = 1, scrcount do
        local f = io.open(memorial.tags .. "." .. s, "w+")
        if f then
            local tags = awful.tag.gettags(s)
            for _, tag in ipairs(tags) do
                f:write(tag.name .. "\n")
            end
            f:close()
        end
        f = io.open(memorial.tags .. "-selected." .. s, "w+")
        if f then
            f:write(awful.tag.getidx() .. "\n")
            f:close()
        end
    end
    -- are we restarting?
    if not restart then
        awful.util.spawn.with_shell("rm -rf " .. memorial.client_tags)
        awful.util.spawn.with_shell("rm -rf " .. memorial.tags .. '*')
    else
        awful.util.mkdir(memorial.client_tags)
        for _, c in ipairs(client.get()) do
            local client_id = c.pid .. '-' .. c.window
            local f = io.open(memorial.client_tags .. '/' .. client_id, 'w+')
            if f then
                for _, t in ipairs(c:tags()) do
                    f:write(t.name .. "\n")
                end
                f:close()
            end
        end
    end
end

function memorial.recall()
    -- test whether screen 1 tag file exists
    local f = io.open(memorial.tags .. ".0", "r")

    if f then
        local old_scr_count = tonumber(f:read("*l"))
        f:close()
        os.remove(memorial.tags .. ".0")
        local new_scr_count = screen.count()
        local count = {}
        local scr_count = math.min(new_scr_count, old_scr_count)

        if scr_count>0 then
            for s = 1, scr_count do
                count[s] = 1
            end
            for s = 1, old_scr_count do
                local count_index = math.min(s, scr_count)
                local fname = memorial.tags .. "." .. s
                local f = io.open(fname, "r")
                if f then 
                    f:close()
                    for tagname in io.lines(fname) do
                        local tag = awful.tag.add(tagname,
                        {
                            screen = count_index,
                            layout = memorial.defaults.layout,
                            mwfact = memorial.defaults.mwfact,
                            nmaster = memorial.defaults.nmaster,
                            ncol = memorial.defaults.ncol,
                        }
                        )
                        awful.tag.move(count[count_index], tag)
                        count[count_index] = count[count_index]+1
                    end
                end
                os.remove(fname)
            end
        end

        for s = 1, screen.count() do
            local tags = awful.tag.gettags(s)
            if #tags >= 1 then
                local fname = memorial.tags .. "-selected." .. s 
                f = io.open(fname, "r")
                if f then
                    local tag = awful.tag.gettags(s)[tonumber(f:read("*l"))]
                    if tag then
                        awful.tag.viewonly(tag)
                    end
                    f:close()
                end
                os.remove(fname)
            else
                local tag = awful.tag.add(
                type(memorial.defaults.name) == "function"
                    and memorial.defaults.name(s) or memorial.defaults.name,
                {
                    screen = s,
                    layout = memorial.defaults.layout,
                    mwfact = memorial.defaults.mwfact,
                    nmaster = memorial.defaults.nmaster,
                    ncol = memorial.defaults.ncol,
                } 
                )
                awful.tag.viewonly(tag)
            end
        end

    else

        for s = 1, screen.count() do
            local tags = awful.tag.gettags(s)
            if #tags < 1 then
                local tag = awful.tag.add(
                type(memorial.defaults.name) == "function"
                    and memorial.defaults.name(s) or memorial.defaults.name,
                {
                    screen = s,
                    layout = memorial.defaults.layout,
                    mwfact = memorial.defaults.mwfact,
                    nmaster = memorial.defaults.nmaster,
                    ncol = memorial.defaults.ncol,
                } 
                )
                awful.tag.viewonly(tag)
            end
        end

    end

end

function memorial.client_manage_tag(c, startup)
    local function name2tags(name, scr)
        local ret = {}
        local a, b = scr or 1, scr or screen.count()

        for s = a, b do
            for _, t in ipairs(awful.tag.gettags(s)) do
                if name == t.name then
                    table.insert(ret, t)
                end
            end
        end

        if #ret > 0 then return ret end
    end

    local function name2tag(name, scr, idx)
        local ts = name2tags(name, scr)
        if ts then return ts[idx or 1] end
    end

    if startup then
        local client_id = c.pid .. '-' .. c.window
        local fname = memorial.client_tags .. '/' .. client_id
        local f = io.open(fname, 'r')

        if f then
            local tags = {}
            for tag in io.lines(fname) do
                tags = gears.table.join(tags, {name2tag(tag)})
            end
            os.remove(fname)
            if #tags>0 then
                c:tags(tags)
                c.screen = awful.tag.getscreen(tags[1])
                awful.placement.no_overlap(c)
                awful.placement.no_offscreen(c)
            end
        end

    end
end

function memorial.init(recall)
    if recall == nil then recall = true end

    client.connect_signal("manage", memorial.client_manage_tag)
    awesome.connect_signal("exit", memorial.save)
    if recall then memorial.recall() end
end

return memorial
