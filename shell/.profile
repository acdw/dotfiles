set -a

PATH="$HOME/.local/bin:$HOME/.cabal/bin:$HOME/.cargo/bin:$PATH:/usr/local/games"
MANPATH="$HOME/.local/share/man:/usr/share/man:/usr/local/share/man"

PAGER="$HOME/.local/bin/nvimpager"
MANPAGER="$HOME/.local/bin/nvimpager"

BEANCOUNT_FILE=$HOME/ledger/ledger.beancount

SHELL=/usr/bin/fish

# Start the ssh agent
# cf https://stackoverflow.com/questions/18880024/start-ssh-agent-on-login
eval $(ssh-agent -s)

# Get luarocks path
eval $(luarocks path)

if [ -d ${XDG_CONFIG_HOME:-$HOME/.config}/profile ]; then
    for file in ${XDG_CONFIG_HOME:-$HOME/.config}/profile/*; do
        . "$file"
    done
    unset file
fi

export PATH="$HOME/.cargo/bin:$PATH"
