# Defined in /tmp/fish.mAhIyN/xq.fish @ line 2
function xq
	if string match -qr '^-' -- $argv[1]
        xbps-query $argv
    else
        xbps-query -Rs $argv
    end
end
