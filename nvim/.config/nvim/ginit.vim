" ginit.vim
" Special initializations for a gui
" by Case Duckworth <acdw@acdw.net>

echom "GINIT"

if exists("g:GuiLoaded")
    Guifont! Go Mono:h10
endif
