" plugins.vim
" plugin-specific configs
" by Case Duckworth <acdw@acdw.net>

let $MYPLUGRC = "$HOME/.config/nvim/plugins.vim"

if empty($XDG_DATA_HOME)
    let g:dir_plug = $HOME . '/.local/share/vim-plugins'
else
    let g:dir_plug = $XDG_DATA_HOME . '/vim-plugins'
endif
let g:plug_plug = g:dir_conf . '/autoload/plug.vim'
silent! call mkdir(g:dir_plug, 'p')

if empty(glob(g:plug_plug))
    exe 'silent !curl -fLo' g:plug_plug '--create-dirs'
                \ 'https://raw.githubusercontent.com/'
                \.'junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(g:dir_plug)
augroup vimplugins
au!

" Asynchronous Lint Engine
Plug 'w0rp/ale'
let g:ale_lint_on_text_changed = 'normal'
let g:ale_sign_error = '!!'
let g:ale_sign_warning = '--'
let g:ale_sign_info  = '..'
let g:ale_sign_style_error = '>>'
let g:ale_sign_style_warning = '=='
nmap <silent> ]a <Plug>(ale_next_wrap)
nmap <silent> [a <Plug>(ale_previous_wrap)
nmap <silent> <Leader>a <Plug>(ale_detail)
let g:ale_linters = {
            \ 'sh': ['shellcheck'],
            \ 'beancount': ['bean_check'],
            \ 'python': ['flake8'],
            \ 'rust': ['cargo'],
            \ 'haskell': ['hdevtools'],
            \ }
let g:ale_fixers = {
            \ 'python': [
            \   'autopep8',
            \   'isort',
            \   'yapf',
            \ ],
            \ }
let g:ale_fix_on_save = 1

" Extended f, F, t, T key mappings
Plug 'rhysd/clever-f.vim'
let g:clever_f_smart_case = 1           " smartly ignore case
let g:clever_f_fix_key_direction = 1    " f -> ; F <-

" Tim Pope's git wrapper
Plug 'tpope/vim-fugitive'
function! Fugitive_head_wrap() abort
    if !exists('b:git_dir')
        return ''
    endif
    return '[±' . fugitive#head(6) . ']'
endfunction
set statusline+=%{Fugitive_head_wrap()}

" A better file browser
Plug 'justinmk/vim-dirvish'
au FileType dirvish call fugitive#detect(@%)

" Autocmd for FocusLost -- it's complicated.
" First install urxvt-ext-evolved:
" https://github.com/amerlyq/urxvt-ext-evolved
" - you need to add the plugin to Xresources
"   URxvt*perl-ext-common: ...,focus-events
" Then install this plugin and add an autocmd
" TODO: get rid of everything I don't want
Plug 'amerlyq/vim-focus-autocmd'

" Colorschemes
" Plug 'ajgrf/parchment' " based on acme and Leuven
" Plug 'rakr/vim-two-firewatch' " blend between duotone light and firewatch
" Plug 'jeffkreeftmeijer/vim-dim' " Default IMproved
" Plug 'zefei/cake16' " CAKE

" Beancount filetype
Plug 'nathangrigg/vim-beancount'
let g:beancount_separator_col = 58
au FileType beancount inoremap <buffer> . .<Esc>:AlignCommodity<CR>a

" Completion manager
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
au BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
set shortmess+=c
au TextChangedI * call ncm2#auto_trigger()
inoremap <expr> <CR> pumvisible() ? "\<C-y>\<CR>" : "\<CR>"
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" Wrap beancount omnifunc
au User Ncm2Plugin call ncm2#register_source({
            \ 'name': 'beancount',
            \ 'priority': 9,
            \ 'subscope_enable': 1,
            \ 'scope': ['beancount'],
            \ 'mark': 'beancount',
            \ 'word_pattern': '[\w:]+',
            \ 'on_complete': ['ncm2#on_complete#omni', 'beancount#complete'],
            \ })

" Pandoc
Plug 'vim-pandoc/vim-pandoc' " Main pandoc plugin
let g:pandoc#formatting#mode = 'h'
let g:pandoc#formatting#textwidth = &tw
let g:pandoc#folding#level = 99
let g:pandoc#folding#fdc = 0
Plug 'vim-pandoc/vim-pandoc-syntax' " Pandoc syntax
let g:pandoc#syntax#conceal#use = 0
Plug 'vim-pandoc/vim-markdownfootnotes' " ease-of-use footnote inserting

" Rust
Plug 'rust-lang/rust.vim'
let g:rustc_path = "/usr/bin/rustc"
let g:rust_recommended_style = 0
let g:rustfmt_autosave = 1

" Filetypes
Plug 'dag/vim-fish' " Fish shell
Plug 'Glench/Vim-Jinja2-Syntax' " jinja2 templating engine
Plug 'tbastos/vim-lua' " Lua 5.3
Plug 'neovimhaskell/haskell-vim' "Haskell
Plug 'JulesWang/css.vim' " CSS 3

" Others
Plug 'benizi/vim-automkdir' " Automatically make dirs when saving
Plug 'jiangmiao/auto-pairs' " Automatically close (), {}, [], '', etc.
Plug 'junegunn/vim-slash' " Automatically clear search highlights
Plug 'tommcdo/vim-exchange' " Text exchanging operators
Plug 'tommcdo/vim-lion' " Alignment operator
Plug 'tpope/vim-commentary' " Commenting keybinds
Plug 'tpope/vim-eunuch' " Helpers for UNIX
Plug 'tpope/vim-rsi' " Readline style insertion for the commandline
Plug 'tpope/vim-surround' " Surrounding operators
Plug 'tpope/vim-speeddating' " C-a, C-x for dates
" Plug 'tpope/vim-endwise' " Add 'end' to various structures
Plug 'alvan/vim-closetag' " Automatically close HTML/XML tags
Plug 'raghur/vim-ghost', {'do': ':GhostInstall'} " Edit textareas with nvim

" Development
" Plug '~/dev/vim/pandoc-wordcount.vim'
" set statusline+=wc[%{pdcwc#statusline()}]

augroup END
call plug#end()
