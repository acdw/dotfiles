" Pager.vim
" Customizations for nvimpager

set laststatus=0
set ruler
set nocursorline
set ignorecase
set smartcase
set title

hi Comment cterm=italic gui=italic ctermfg=242 guifg=Grey40
hi LineNr ctermbg=254 ctermfg=237 guibg=Grey90 guifg=Grey20
hi CursorLine guibg=NONE cterm=NONE
hi CursorLineNr ctermbg=242 ctermfg=white guibg=Grey40 guifg=white
hi Search ctermbg=226 guibg=Yellow

nnoremap j <C-e>
nnoremap k <C-y>
