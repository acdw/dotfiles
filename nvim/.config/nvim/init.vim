" init.vim
" v. 3 : Flirting with nothingness
" by Case Duckworth <acdw@acdw.net>
" vim: fdm=marker

" Preamble {{{
filetype plugin indent on
syntax enable
" }}}
" Settings {{{
set autoindent
set autoread
set autowriteall
set backspace=indent,eol,start
set backupskip=/tmp/*,$TMPDIR/*,$TMP/*,$TEMP/*,*/shm/*
set belloff=all
set breakindent
set colorcolumn=+1
set complete-=i
set confirm
set cursorline
set display+=lastline
set equalalways
set exrc
set formatoptions=tcqj
set gdefault
set history=10000
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set linebreak
set list
set listchars=tab:\,.,nbsp:~,extends:>,precedes:<
set modeline
set mouse=a
set nrformats=alpha,hex,bin
set number
set numberwidth=4
set ruler
set secure
set sessionoptions-=options
set shortmess=aoOTIc
set showbreak=\\
set showcmd
set sidescrolloff=1
set scrolloff=3
set smartcase
set smarttab
set spelllang=en_us
set splitbelow
set splitright
set switchbuf=usetab
set tabpagemax=50
set textwidth=78
set title
set undofile
set undolevels=10000
set updatecount=10
set updatetime=500
set viewoptions=folds,cursor,slash,unix
set wildignorecase
set wildmenu

set tabstop=4
set expandtab
let &shiftwidth = &tabstop
let &softtabstop = &tabstop
set shiftround

if has('nvim')
    set shada^=!
    set inccommand=nosplit
    if $TERM_=~ 'kitty'     " include other possibilities here
        set termguicolors
    endif
    let &packpath = &runtimepath
    if empty($XDG_DATA_HOME)
        let g:dir_data = $HOME . "/.local/share/nvim"
    else
        let g:dir_data = $XDG_DATA_HOME . "/nvim"
    endif
    if empty($XDG_CONFIG_HOME)
        let g:dir_conf = $HOME . "/.config/nvim"
    else
        let g:dir_conf = $XDG_CONFIG_HOME . "/nvim"
    endif
else
    set encoding=utf-8
    set langnoremap
    set nocompatible
    set ttyfast
    set viminfo^=!
    if &&ttimeoutlen == -1
        set ttimeout
        set ttimeoutlen=100
    endif
    let g:dir_data = $HOME . "/.vim"
    let g:dir_conf = $HOME . "/.vim"
endif

let &backupdir = g:dir_data . "/backup"
let &directory = g:dir_data . "/swap//"
let &spellfile = g:dir_conf . "/spell/en.utf8.add"
let &undodir = g:dir_data . "/undo"
let &viewdir = g:dir_data . "/view"

for dir in [&backupdir, &directory, &undodir, &viewdir,
            \fnamemodify(&spellfile, ":p:h")]
    silent! call mkdir(dir, 'p')
endfor

if executable("rg")
    set grepprg=rg\ --vimgrep
    set grepformat^=%f:%l:%c:%m
elseif executable("ag")
    set grepprg=ag\ --vimgrep
    set grepformat^=%f:%l:%c:%m
endif

let g:is_bash = 1

if &shell =~# 'fish$'
    set shell=sh
endif
" }}}
" Maps {{{
" \<Space> is space
let mapleader = ","
let maplocalleader = ","

noremap ; :

nnoremap ' `
nnoremap ` '

nnoremap K i<CR><Esc>d^kg_lD
nnoremap <Leader>h K

nnoremap Q gqip
xnoremap Q gq

nnoremap <Leader>so vip:sort<CR>
xnoremap <Leader>so :sort<CR>

nnoremap <C-j> <C-w>w
nnoremap <C-k> <C-w>W
nnoremap <BS> <C-^>

nnoremap <expr> j (v:count > 1 ? 'm`' . v:count . 'j' : 'gj')
nnoremap <expr> k (v:count > 1 ? 'm`' . v:count . 'k' : 'gk')

inoremap <F5> <C-r>=strftime('%F')<CR>
" }}}
" Auocommands {{{
augroup vimrc
    au!
    au BufWinEnter * silent! loadview | silent! lcd %:p:h
    au BufWinLeave * silent! mkview
    au FocusLost * silent! wall
    au BufWritePre * silent! %s/\s\+$//
augroup END

augroup quickfix
    au!
    au QuickFixCmdPost [^l]* cwindow
    au QuickFixCmdPost l* lwindow
    au VimEnter * cwindow
augroup END

augroup ft_pandoc
    au!
    au FileType pandoc,markdown setl spell tw=78 cc=+1
augroup END

source ~/.local/share/nvim/beancount_abbrev.vim
augroup ft_beancount
    au!
    au BufReadPre,VimEnter *.beancount call FT_beancount_abbrev()
augroup END
" }}}
" Plugins & Statusline {{{
set statusline=%2n%{mode()}\ %q%f\ %y%m\ %a%=

let g:pluginsrc = g:dir_conf . "/plugins.vim"
if !empty(glob(g:pluginsrc))
    exe "source" g:pluginsrc
endif

set statusline+=\ %-10.(%l:%c%V%)%-4P
" }}}
" Functions {{{

" https://www.reddit.com/r/vim/comments/7yn0xa/editing_macros_easily_in_vim/duie7s4/?
function! ChangeRegister() abort
    let x = nr2char(getchar())
    call feedkeys("q:ilet @" . x . " = \<C-r>\<C-r>=string(@" . x . ")\<CR>\<Esc>0f'", 'n')
endfunction
nnoremap <Leader>cr :call ChangeRegister()<CR>
" }}}
" Fixes {{{
if exists("$DISPLAY")
    if !has("nvim")
        " Make cursors look nice in terminals
        let &t_SI = "\<Esc>[6 q"
        let &t_SR = "\<Esc>[4 q"
        let &t_EI = "\<Esc>[2 q"
    endif
endif
" }}}
" Colors {{{
hi Comment cterm=italic ctermfg=23 ctermbg=195
hi Type ctermfg=Blue
hi Identifier ctermfg=23
hi LineNr ctermbg=NONE ctermfg=15
hi CursorLine guibg=NONE cterm=NONE
hi CursorLineNr ctermbg=7 ctermfg=8
hi Search ctermbg=226 guibg=Yellow
hi Todo ctermbg=226 guibg=Yellow ctermfg=14 cterm=italic
" }}}
