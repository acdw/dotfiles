# QUTEBROWSER config.py
# pylint: disable=C0111

import os
import subprocess

c = c  # noqa: F821 pylint: disable=E0602,C0103
config = config  # noqa: F821 pylint: disable=E0602,C0103

# Command aliases
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save'}

# Enable javascript.  Optional 3rd argument takes a site regex
config.set('content.javascript.enabled', True)
# Default encoding
c.content.default_encoding = 'utf-8'
c.content.geolocation = False

# Save downloads here.
c.downloads.location.directory = '~/dl'
# Position
c.downloads.position = 'bottom'

# Hints
c.hints.mode = 'number'  # can also type letters from hinted element to filter

# Keyhints
c.keyhint.delay = 30

# Editor (and args) for :open-editor. The following placeholders apply:
# {file} : Filename to be edited
# {line} : line in which the caret is found in the text
# {column} : column of caret in the text
# {line0} : same as {line}, but from index 0
# {column0} : {column} :: {line0} : {line}
c.editor.command = [
    os.getenv('EDITOR', 'editor'), '-f', '{file}', '-c',
    'normal {line}G{column0}l'
]
# Format for the window title.
c.window.title_format = ''.join([
    '{perc}', '{title}', '{title_sep}', '{current_url}', '{title_sep}',
    'qutebrowser'
])
# Show scrollbar
c.scrolling.bar = 'always'

# Fonts
c.fonts.monospace = 'Go Mono'
c.fonts.statusbar = '9pt monospace'
c.fonts.tabs = '9pt monospace'

# Colors


def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l: l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props


xresources = read_xresources("*")

# Tabs
c.tabs.last_close = 'close'  # close window after last tab closed
c.tabs.tabs_are_windows = False
c.tabs.show = 'multiple'

# Opens
c.url.default_page = 'about:blank'
c.url.start_pages = 'about:blank'
c.auto_save.session = True

# Binds
config.bind(';v', 'hint links spawn mpv {hint-url}')
if c.tabs.tabs_are_windows:
    config.bind('J', "spawn qute-wm-tab 1")
    config.bind('K', "spawn qute-wm-tab -1")
