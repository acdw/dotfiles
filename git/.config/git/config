[user]
    email = acdw@acdw.net
    name = Case Duckworth

[push]
    default = simple

[core]
    editor = nvim
    precomposeunicode = true
    pager = less

[merge]
    conflictstyle = diff3

[alias]
    # Easier locations
    root = rev-parse --show-toplevel
    current-branch = rev-parse --abbrev-ref HEAD
    # Easier listing and info
    branches = branch -a
    tags = tag -l
    stashes = stash list
    remotes = remote -v
    staged = diff --cached
    graph = log --graph -10 --branches --remotes --tags --format=format:'%Cgreen%h %Creset: %<(75,trunc)%s (%cN, %cr) %Cred%d' --date-order
    precommit = diff --cached --diff-algorithm=minimal -w
    # Easier actions
    discard = checkout --
    uncommit = reset --soft HEAD^
    unstage = reset HEAD --
    amend = commit --amend
    pushall = !git remote | xargs -L1 git push --all
    # Shortened commonalities
    st = status -bs

# Better urls
[url "https://github.com/"]
    insteadOf = "gh:"
[url "git@github.com:"]
    pushInsteadOf = "gh:"

[url "https://gitlab.com/"]
    insteadOf = "gl:"
[url "git@gitlab.com:"]
    pushInsteadOf = "gl:"

; [credential]
; 	helper = /home/case/.local/bin/pass-git-helper
; 	useHttpPath = true

[bash]
    showUntrackedFiles = true
    showDirtyState = true
